package zero

import (
	"bitbucket.org/playsure-go/convert"
	"reflect"
)

func StringOrDefault(value interface{}, defaultValue string) string {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := ""
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.String:
			result = elem.String()
		}
	case reflect.String:
		result = value.(string)
	}
	if result == "" {
		result = defaultValue
	}
	return result
}

func IntOrDefault(value interface{}, defaultValue int) int {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := 0
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32:
			result = int(elem.Int())
		}
	case reflect.Int:
		result = value.(int)
	case reflect.Int8:
		result = int(value.(int8))
	case reflect.Int16:
		result = int(value.(int16))
	case reflect.Int32:
		result = int(value.(int32))
	case reflect.String:
		i, e := convert.StringToInt(value.(string))
		if e == nil { // An integer.
			result = i
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Int8OrDefault(value interface{}, defaultValue int8) int8 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := int8(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Int8:
			result = int8(elem.Int())
		}
	case reflect.Int8:
		result = value.(int8)
	case reflect.String:
		i, e := convert.StringToInt8(value.(string))
		if e == nil { // An 8-bits integer.
			result = i
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Int16OrDefault(value interface{}, defaultValue int16) int16 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := int16(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Int8,
			reflect.Int16:
			result = int16(elem.Int())
		}
	case reflect.Int8:
		result = int16(value.(int8))
	case reflect.Int16:
		result = value.(int16)
	case reflect.String:
		i, e := convert.StringToInt16(value.(string))
		if e == nil { // An 16-bits integer.
			result = i
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Int32OrDefault(value interface{}, defaultValue int32) int32 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := int32(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32:
			result = int32(elem.Int())
		}
	case reflect.Int:
		result = int32(value.(int))
	case reflect.Int8:
		result = int32(value.(int8))
	case reflect.Int16:
		result = int32(value.(int16))
	case reflect.Int32:
		result = value.(int32)
	case reflect.String:
		i, e := convert.StringToInt32(value.(string))
		if e == nil { // An 32-bits integer.
			result = i
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Int64OrDefault(value interface{}, defaultValue int64) int64 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := int64(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			result = elem.Int()
		}
	case reflect.Int:
		result = int64(value.(int))
	case reflect.Int8:
		result = int64(value.(int8))
	case reflect.Int16:
		result = int64(value.(int16))
	case reflect.Int32:
		result = int64(value.(int32))
	case reflect.Int64:
		result = value.(int64)
	case reflect.String:
		i, e := convert.StringToInt64(value.(string))
		if e == nil { // An 64-bits integer.
			result = i
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func UintOrDefault(value interface{}, defaultValue uint) uint {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := uint(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32:
			result = uint(elem.Uint())
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32:
			i := elem.Int()
			if i > 0 {
				result = uint(i)
			}
		}
	case reflect.Uint:
		result = value.(uint)
	case reflect.Uint8:
		result = uint(value.(uint8))
	case reflect.Uint16:
		result = uint(value.(uint16))
	case reflect.Uint32:
		result = uint(value.(uint32))
	case reflect.Int:
		i := value.(int)
		if i > 0 {
			result = uint(i)
		}
	case reflect.Int8:
		i := value.(int8)
		if i > 0 {
			result = uint(i)
		}
	case reflect.Int16:
		i := value.(int16)
		if i > 0 {
			result = uint(i)
		}
	case reflect.Int32:
		i := value.(int32)
		if i > 0 {
			result = uint(i)
		}
	case reflect.String:
		ui, e := convert.StringToUint(value.(string))
		if e == nil { // An unsigned integer.
			result = ui
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Uint8OrDefault(value interface{}, defaultValue uint8) uint8 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := uint8(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Uint8:
			result = uint8(elem.Uint())
		case reflect.Int8:
			i := elem.Int()
			if i > 0 {
				result = uint8(i)
			}
		}
	case reflect.Uint8:
		result = value.(uint8)
	case reflect.Int8:
		i := value.(int8)
		if i > 0 {
			result = uint8(i)
		}
	case reflect.String:
		ui, e := convert.StringToUint8(value.(string))
		if e == nil { // An 8-bits unsigned integer.
			result = ui
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Uint16OrDefault(value interface{}, defaultValue uint16) uint16 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := uint16(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Uint8,
			reflect.Uint16:
			result = uint16(elem.Uint())
		case reflect.Int8,
			reflect.Int16:
			i := elem.Int()
			if i > 0 {
				result = uint16(i)
			}
		}
	case reflect.Uint8:
		result = uint16(value.(uint8))
	case reflect.Uint16:
		result = value.(uint16)
	case reflect.Int8:
		i := value.(int8)
		if i > 0 {
			result = uint16(i)
		}
	case reflect.Int16:
		i := value.(int16)
		if i > 0 {
			result = uint16(i)
		}
	case reflect.String:
		ui, e := convert.StringToUint16(value.(string))
		if e == nil { // An 16-bits unsigned integer.
			result = ui
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Uint32OrDefault(value interface{}, defaultValue uint32) uint32 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := uint32(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32:
			result = uint32(elem.Uint())
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32:
			i := elem.Int()
			if i > 0 {
				result = uint32(i)
			}
		}
	case reflect.Uint:
		result = uint32(value.(uint))
	case reflect.Uint8:
		result = uint32(value.(uint8))
	case reflect.Uint16:
		result = uint32(value.(uint16))
	case reflect.Uint32:
		result = value.(uint32)
	case reflect.Int:
		i := value.(int)
		if i > 0 {
			result = uint32(i)
		}
	case reflect.Int8:
		i := value.(int8)
		if i > 0 {
			result = uint32(i)
		}
	case reflect.Int16:
		i := value.(int16)
		if i > 0 {
			result = uint32(i)
		}
	case reflect.Int32:
		i := value.(int32)
		if i > 0 {
			result = uint32(i)
		}
	case reflect.String:
		ui, e := convert.StringToUint32(value.(string))
		if e == nil { // An 32-bits unsigned integer.
			result = ui
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Uint64OrDefault(value interface{}, defaultValue uint64) uint64 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := uint64(0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32,
			reflect.Uint64:
			result = elem.Uint()
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			i := elem.Int()
			if i > 0 {
				result = uint64(i)
			}
		}
	case reflect.Uint:
		result = uint64(value.(uint))
	case reflect.Uint8:
		result = uint64(value.(uint8))
	case reflect.Uint16:
		result = uint64(value.(uint16))
	case reflect.Uint32:
		result = uint64(value.(uint32))
	case reflect.Uint64:
		result = value.(uint64)
	case reflect.Int:
		i := value.(int)
		if i > 0 {
			result = uint64(i)
		}
	case reflect.Int8:
		i := value.(int8)
		if i > 0 {
			result = uint64(i)
		}
	case reflect.Int16:
		i := value.(int16)
		if i > 0 {
			result = uint64(i)
		}
	case reflect.Int32:
		i := value.(int32)
		if i > 0 {
			result = uint64(i)
		}
	case reflect.Int64:
		i := value.(int64)
		if i > 0 {
			result = uint64(i)
		}
	case reflect.String:
		ui, e := convert.StringToUint64(value.(string))
		if e == nil { // An 64-bits unsigned integer.
			result = ui
		}
	}
	if result == 0 {
		result = defaultValue
	}
	return result
}

func Float32OrDefault(value interface{}, defaultValue float32) float32 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := float32(0.0)
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Float32:
			result = float32(elem.Float())
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32,
			reflect.Uint64:
			result = float32(elem.Uint())
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			i := elem.Int()
			if i > 0 {
				result = float32(uint64(i))
			}
		}
	case reflect.Float32:
		result = value.(float32)
	case reflect.Uint:
		result = float32(value.(uint))
	case reflect.Uint8:
		result = float32(value.(uint8))
	case reflect.Uint16:
		result = float32(value.(uint16))
	case reflect.Uint32:
		result = float32(value.(uint32))
	case reflect.Uint64:
		result = float32(value.(uint64))
	case reflect.Int:
		result = float32(value.(int))
	case reflect.Int8:
		result = float32(value.(int8))
	case reflect.Int16:
		result = float32(value.(int16))
	case reflect.Int32:
		result = float32(value.(int32))
	case reflect.Int64:
		result = float32(value.(int64))
	case reflect.String:
		f, e := convert.StringToFloat32(value.(string))
		if e == nil { // An 32-bits float.
			result = f
		}
	}
	if result == 0.0 {
		result = defaultValue
	}
	return result
}

func Float64OrDefault(value interface{}, defaultValue float64) float64 {
	if value == nil {
		return defaultValue
	}
	valueType := reflect.TypeOf(value)
	if valueType == nil {
		return defaultValue
	}
	result := 0.0
	kind := valueType.Kind()
	switch kind {
	case reflect.Ptr:
		elem := reflect.ValueOf(value).Elem()
		switch elem.Kind() {
		case reflect.Float32,
			reflect.Float64:
			result = elem.Float()
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32,
			reflect.Uint64:
			result = float64(elem.Uint())
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			i := elem.Int()
			if i > 0 {
				result = float64(uint64(i))
			}
		}
	case reflect.Float32:
		result = float64(value.(float32))
	case reflect.Float64:
		result = value.(float64)
	case reflect.Uint:
		result = float64(value.(uint))
	case reflect.Uint8:
		result = float64(value.(uint8))
	case reflect.Uint16:
		result = float64(value.(uint16))
	case reflect.Uint32:
		result = float64(value.(uint32))
	case reflect.Uint64:
		result = float64(value.(uint64))
	case reflect.Int:
		result = float64(value.(int))
	case reflect.Int8:
		result = float64(value.(int8))
	case reflect.Int16:
		result = float64(value.(int16))
	case reflect.Int32:
		result = float64(value.(int32))
	case reflect.Int64:
		result = float64(value.(int64))
	case reflect.String:
		f, e := convert.StringToFloat64(value.(string))
		if e == nil { // An 64-bits float.
			result = f
		}
	}
	if result == 0.0 {
		result = defaultValue
	}
	return result
}
