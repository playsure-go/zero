package zero

func StringOrZero(value interface{}) string {
	return StringOrDefault(value, "")
}

func IntOrZero(value interface{}) int {
	return IntOrDefault(value, 0)
}

func Int8OrZero(value interface{}) int8 {
	return Int8OrDefault(value, 0)
}

func Int16OrZero(value interface{}) int16 {
	return Int16OrDefault(value, 0)
}

func Int32OrZero(value interface{}) int32 {
	return Int32OrDefault(value, 0)
}

func Int64OrZero(value interface{}) int64 {
	return Int64OrDefault(value, 0)
}

func UintOrZero(value interface{}) uint {
	return UintOrDefault(value, 0)
}

func Uint8OrZero(value interface{}) uint8 {
	return Uint8OrDefault(value, 0)
}

func Uint16OrZero(value interface{}) uint16 {
	return Uint16OrDefault(value, 0)
}

func Uint32OrZero(value interface{}) uint32 {
	return Uint32OrDefault(value, 0)
}

func Uint64OrZero(value interface{}) uint64 {
	return Uint64OrDefault(value, 0)
}

func Float32OrZero(value interface{}) float32 {
	return Float32OrDefault(value, 0.0)
}

func Float64OrZero(value interface{}) float64 {
	return Float64OrDefault(value, 0.0)
}
